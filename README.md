# react-redux-todo-ts

This is an example app using typescript, react and redux.

You can find an article explaining each parts of the app here : 

!!  https://medium.com/@nem121/todo-app-with-typescript-redux-e6a4c2f02079
!!  https://github.com/Nemak121/react-redux-todo-ts/blob/master/package.json
https://javascriptplayground.com/react-typescript/


## Start the application 
You need to be at the root of the application folder and do : 

`npm install && npm start`

## Logger 
If you open a debugging console in the browser, you should see every action logged into the console.